module.exports = function (app) {
	app.service('ajaxService', ajaxService);
}


/*=============================
=            LOGIC            =
=============================*/



function ajaxService($http, $q) {
	return {

		getNavV: getNavV,
		getNavH: getNavH,
		getContent: getContent,
		colors: colors
	}

	/////////////////////////////////////////////////

	function getContent(h, v) {
		var deferred = $q.defer();


		var content = [
			['<div><div class="CSPvNext "><div class="row-fluid" data-view4="1" data-view3="1" data-view2="1" data-view1="1" data-cols="1"><div class="feature span"><a target="_self" class="mscom-link" href="http://www.microsoftstore.com/store/msmea/uk_UA/home/ThemeID.27659900/Currency.UAH/mktp.UA" bi:pgarea="body" bi:cmpgrp="for home" bi:cmpnm="for home" bi:linkid="SSAGPTSS_4382_13725_141_a" bi:linktype="marcom" bi:interactiontype="1" bi:index="0"><img src="https://c.s-microsoft.com/uk-ua/CMSImages/Store_Family_0303_540x304_EN_US.jpg?version=9950caf6-9b90-551a-1b3c-f4856a7b157c" class="mscom-image feature-image" alt="Безкоштовна доставка для всіх продуктів, придбаних у Магазині Microsoft." width="540" height="304"><div class="feature-text-overlay" style="height: 132.2px; min-height: 1px;"><strong></strong><h3 class="feature-title">Магазин Microsoft</h3><strong></strong><p class="feature-description">Безкоштовна доставка для всіх продуктів, придбаних у Магазині Microsoft.</p><strong></strong></div></a></div></div></div><div>','2','3','4','5'],
			['<div><video id="video1" style="width:500px;max-width:100%;" controls=""> <source src="http://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4"> <source src="http://www.w3schools.com/html/mov_bbb.ogg" type="video/ogg"> Your browser does not support HTML5 video. </video>','<video id="video1" style="width:600px;max-width:100%;" controls=""> <source src="http://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4"> <source src="http://www.w3schools.com/html/mov_bbb.ogg" type="video/ogg"> Your browser does not support HTML5 video. </video>','<video id="video1" style="width:600px;max-width:100%;" controls=""> <source src="http://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4"> <source src="http://www.w3schools.com/html/mov_bbb.ogg" type="video/ogg"> Your browser does not support HTML5 video. </video><div>', '2', 'one more'],
			['<div><div class="flex-33" flex="33" style="color: rgb(26, 35, 38); font-family: Roboto, , sans-serif; font-size: 14px; padding: 0px 24px; background-color: rgb(255, 255, 255);"><h3 class="text-headline" style="margin: 0px 0px 16px; font-size: 24px; opacity: 0.87; line-height: 32px;">Fast</h3><p class="text-body" style="margin: 0px 0px 16px; font-size: 16px; opacity: 0.87; line-height: 28px; padding: 0px;">Angular computes updates based on changes to data, not DOM, for fast updates that scale to the largest data sets with minimal memory overhead.</p></div><div class="flex-33" flex="33" style="color: rgb(26, 35, 38); font-family: Roboto, sans-serif; font-size: 14px; padding: 0px 24px; background-color: rgb(255, 255, 255);"><h3 class="text-headline" style="margin: 0px 0px 16px; font-size: 24px; opacity: 0.87; line-height: 32px;">Mobile</h3><p class="text-body" style="margin: 0px 0px 16px; font-size: 16px; opacity: 0.87; line-height: 28px; padding: 0px;">With Angular Universal for server-side rendering and Web Workers for smooth scrolling and transitions, Angular 2 solves the core challenges in mobile web performance.</p></div><div class="flex-33" flex="33" style="color: rgb(26, 35, 38); font-family: Roboto, , sans-serif; font-size: 14px; padding: 0px 24px; background-color: rgb(255, 255, 255);"><h3 class="text-headline" style="margin: 0px 0px 16px; font-size: 24px; opacity: 0.87; line-height: 32px;">Flexible</h3><p class="text-body" style="margin: 0px 0px 16px; font-size: 16px; opacity: 0.87; line-height: 28px; padding: 0px;">Supports several languages including plain JavaScript, TypeScript, and Dart. Also supports both object-style data structure with POJO data-binding and functional reactive style with unidirectional data flow and support for observables and immutable data structures.</p></div><p>&nbsp;</p></div>','12','13', '44'],
			['eee','17','18','19'],
			['21','22','23','24'],
			['21','22','23','24']
		];


		try {
			deferred.resolve(content[h][v])
		} catch(err) {
			deferred.reject()
		}
			

		return deferred.promise
	}

	function getNavV(h) {
		var nav = [
			['icon','Reveal','Specs','Configure'],
			['icon','Reveal','Specs','Configure', 'one more'],
			['icon','Reveal','Specs','Configure'],
			['icon','Reveal','Specs','Configure'],
			['icon','Reveal','Specs','Configure'],
			['<img src="image/shop.png" alt="" />','Billing','Shipping','Checkout']
		]

		return nav
	}

	function getNavH() {
		return ['Tab One', 'Tab Two',  'Tab Three', 'Tab Four', 'Tab Five', '<img  src="image/shop.png" alt="" />']
	}

	function colors(h, v) {
		var color = [
			['#453124','blue','23','24','25'],
			['#5f4312','22','23','24','25'],
			['21','22','23','24','25'],
			['21','22','23','24','25'],
			['21','22','23','24','25'],
		]
		return color;
	}

}