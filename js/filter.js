module.exports = function (app) {
	app.filter('sanitize', sanitize);
}


/*=============================
=            LOGIC            =
=============================*/



function sanitize($sce) {
	
	return function(htmlCode){
		return $sce.trustAsHtml(htmlCode);
	}
	
	
}