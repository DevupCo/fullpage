global.Reveal = require('reveal.js');

var angular = require('angular');
require('angular-swipe')

var app = angular.module('app', ['swipe'])

// include components
require('./controller')(app)
require('./service')(app)
require('./filter')(app)

angular.bootstrap(document, ['app'])

// init Reveal


Reveal.initialize({
	controls: false,
	progress: false,
	keyboard: false,
	viewDistance: 6,
	touch: false
})
