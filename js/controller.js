module.exports = function (app) {
	app.controller('indexCtrl', indexCtrl);
}


/*=============================
=            LOGIC            =
=============================*/


function indexCtrl(ajaxService, $timeout, $q) {
	var wm = this;

	// top nav names
	wm.horizTabs = ajaxService.getNavH();
	// bottom nav names
	wm.vertTabs = ajaxService.getNavV();

	// current sliders
	wm.currentH = 0;
	wm.currentV = 0;


	// content storage
	wm.content = {};

	wm.color = ajaxService.colors()

	// get data for first slide
	getFirstSlide(0, 0).then(function () {
		// get all data
		getAllContent();
	})

	/**
	 * move slider on click
	 * @type {undefined}
	 */
	wm.moveSlider = moveSlider;


	///////////////////////////////////


	function moveSlider(h, v) {
		// go to slide h
		while(wm.currentH !== h ) {
			if(h > wm.currentH) {
				delay(Reveal.slide)(++wm.currentH, v)
			} else if(h < wm.currentH) {
				delay(Reveal.slide)(--wm.currentH, v, 'r')
			}
		}

		while(wm.currentV !== v ) {
			if(v > wm.currentV) {
				delay(Reveal.slide, 'v')(h, ++wm.currentV)
			} else if(v < wm.currentV) {
				delay(Reveal.slide, 'v')(h, --wm.currentV, 'r')
			}
		}

		// set current slide
		//wm.currentH = Reveal.getIndices().h;
		//wm.currentV = Reveal.getIndices().v;

	}


	function getContent(h, v) {

		var deferred = $q.defer();

		// search key
		var key = h + ',' + v;

		// if content is not in storage
		if(!wm.content[key]) {
			ajaxService.getContent(h, v).then(function (res) {
				wm.content[key] = res;
				deferred.resolve(true)
			}, function () {
				wm.loading = false;
			})
		} else {
			// content in storage
			deferred.resolve(true)
		} 

		return deferred.promise;
	}

	function getFirstSlide() {
		var deferred = $q.defer();

		// get backgrounds
		wm.color = ajaxService.colors(0, 0);

		// change bottom nav
		wm.vertTabs = ajaxService.getNavV(0);

		//get content 

		ajaxService.getContent(0, 0).then(function(res) {
			wm.content['0,0'] = res;
			deferred.resolve(true)
		})

		return deferred.promise;
	}

	function getAllContent() {
		// for each horiz slide
		angular.forEach(wm.vertTabs, function (valueH, keyH) {
			// for each vert slide
			angular.forEach(valueH, function (valueV, keyV) {
				//get content
				ajaxService.getContent(keyH, keyV).then(function (res) {
					wm.content[keyH + ',' + keyV] = res;
				})

			})

		})
	}

	function delay(cb, vertical) {
		return function (h, v, r) {

			var count = vertical && (v + 1) || h;

			var delay = 0;

			if(r) {
				delay = 300 / count;
			} else {
				delay =  150 * count
			}

			console.log(delay, count)

			$timeout(function() {
				cb(h, v)
			}, delay);
		}
	}


}