var quick = require('gulp-quick');

quick.sync({
	dist: './dist'
});

quick.sass({
	style: 'compressed',
	dist: './dist',
	watch: [
		'./sass/**'
	]
})

quick.js({
	browserify: true,
	// minify: true, // working with browserify
	main: './js/index.js',
	dist: './dist',
	watch: [
		'./js/**'
	]
})

quick.run();